const express = require('express');
const app = express();

/**
 * 
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number} Sum of a and p
 */
const add = (a,b) => {
    return a+ b;
};

app.get('', (req, res) => {
    const sum = add(1,2);
    console.log(sum);
    res.send('ok');
});

app.listen(3000, () => {
    console.log('Server listening to localhost:3000');
});